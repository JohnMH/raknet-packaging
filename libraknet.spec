Name:           libraknet
Version:        4.081
Release:        5%{?dist}
Summary:        Cross platform C++ networking engine

License:        BSD
URL:            https://github.com/OculusVR/RakNet
Source0:        https://github.com/JohnMHarrisJr/RakNet/archive/4.081-3.tar.gz

BuildRequires:  cmake gcc-c++

%description
RakNet is a cross platform, open source, C++ networking engine for
game programmers.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains header files for
developing applications that use %{name}.

%prep
%setup -q -n RakNet-4.081-4

%build
%cmake .
make raknet %{?_smp_mflags}

%install
mkdir -p $RPM_BUILD_ROOT%{_libdir}
cp -p Lib/DLL/libraknet.so.4.081 $RPM_BUILD_ROOT%{_libdir}
ln -rs $RPM_BUILD_ROOT%{_libdir}/libraknet.so.4.081 $RPM_BUILD_ROOT%{_libdir}/libraknet.so
mkdir -p $RPM_BUILD_ROOT%{_includedir}/raknet
cp -p Source/*.h $RPM_BUILD_ROOT%{_includedir}/raknet

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%license LICENSE
%{_libdir}/libraknet.so.4.081
%{_libdir}/libraknet.so.4

%files devel
%{_includedir}/raknet/
%{_libdir}/libraknet.so

%changelog
* Fri May 27 2016 John M. Harris <johnmh@openmailbox.org> - 4.081-5
- Updated spec file for include dir ownership and moved .so to devel.

* Sat Mar 19 2016 John M. Harris <johnmh@openmailbox.org> - 4.081-4
- Updated spec file to include valid changelog and preserve timestamps

* Sat Feb 20 2016 John M. Harris <johnmh@openmailbox.org> - 4.081-3
- Use actual RakNet sources and CMake to generate build files.

* Mon Feb 15 2016 John M. Harris <johnmh@openmailbox.org> - 4.081-2
- Updated spec file to use a wildcard for headers, not for libraries.

* Mon Feb 15 2016 John M. Harris <johnmh@openmailbox.org> - 4.081-1
- Initial packaging of raknet.
